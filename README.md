# stuett

Disclaimer: This project is still under heavy development.

## Quickstart

```
pip install stuett
```

You can find examples on how to use the package [here](https://github.com/ETHZ-TEC/permafrostanalytics/).

## Install from sources
On Linux, you can install the package with

```
pip install poetry
poetry install
```

If you want to install it into a anaconda environment do

```
conda create -n stuett python==3.7 
conda activate stuett
pip install --pre poetry
poetry install
```


Note that there are several issues when installing on Windows
* Make sure git is installed and can be found by Anaconda! You can install git with Anaconda `conda install git curl`
* Install a Microsoft C Compiler, gcc seems to have some issues


## Versions
The versioning scheme is based on a [CalVer](https://calver.org/) using YY.MM.MICRO