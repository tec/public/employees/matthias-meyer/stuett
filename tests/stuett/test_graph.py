"""MIT License

Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich), Matthias Meyer


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

import stuett
import datetime as dt
from tests.stuett.sample_data import *

import pytest

# helper function for non-stuett node
@stuett.dat
def bypass(x):
    return x


class MyNode(stuett.core.StuettNode):
    def __init__(self):
        super().__init__()

    def forward(self, data=None, request=None):
        print(data, request)
        return data + 4

    def configure(self, requests=None):
        requests = super().configure(requests)
        if "start_time" in requests:
            requests["start_time"] += 1
        return requests


class MyMerge(stuett.core.StuettNode):
    def forward(self, data, request):
        return data[0] + data[1]


class MySource(stuett.data.DataSource):
    def __init__(self, start_time=None):
        super().__init__(start_time=start_time)

    def forward(self, data=None, request=None):
        return request["start_time"]


def test_configuration():
    node = MyNode()

    # create a stuett graph
    x_in = node(data=5, delayed=True)  # data input
    x = bypass(x_in)
    x = node(x, delayed=True)

    print(x.compute())

    # x.visualize()
    import dask

    dsk, dsk_keys = dask.base._extract_graph_and_keys([x])
    print(dict(dsk))

    # create a configuration file
    config = {}

    # configure the graph
    x_configured = stuett.core.configuration(
        x, config
    )  # BUG: somehow config cuts the graph off

    import dask

    dsk, dsk_keys = dask.base._extract_graph_and_keys([x_configured])
    print(dsk)

    x = x_configured.compute()

    print(x)
    # TODO: finalize test


def test_datasource():
    source = MySource()
    node = MyNode()

    # create a stuett graph
    x = source(delayed=True)
    x = bypass(x)
    x = node(x, delayed=True)

    # create a configuration file
    config = {"start_time": 0, "end_time": 1}

    # configure the graph
    configured = stuett.core.configuration(x, config)

    x_configured = configured.compute(
        scheduler="single-threaded", rerun_exceptions_locally=True
    )

    assert x_configured == 5


def test_merging():
    source = MySource()
    node = MyNode()
    merge = MyMerge()

    # create a stuett graph
    import dask

    x = source(delayed=True)
    x = bypass(x)
    x = node(x, delayed=True)
    x_b = node(x, delayed=True)
    x = merge([x_b, x], delayed=True)

    # create a configuration file
    config = {"start_time": 0, "end_time": 1}

    # configure the graph
    configured = stuett.core.configuration(x, config)

    x_configured = configured.compute()

    assert x_configured == 14

    # TODO: Test default_merge

    # TODO:

    try:
        """
        This still fails. Configuration cannot handle "apply" nodes in the dask graph.
        The problem arises if we have a branch that needs to merge request and the 
        apply node is the one which receives the two requests. Then it requires a 
        default merge handler, which is not supplied (and does not need to) because the
        underlying function in this case supplies the merger.
        """
        x = source(delayed=True)(dask_key_name="source")
        x = bypass(x)(dask_key_name="bypass")
        x = node(x, delayed=True)(dask_key_name="node")
        x_b = node(x, delayed=True)(dask_key_name="branch")  # branch
        x = merge([x_b, x], delayed=True)(dask_key_name="merge")

        # create a configuration file
        config = {"start_time": 0, "end_time": 1}

        import dask

        dsk, dsk_keys = dask.base._extract_graph_and_keys([x])

        print(dict(dsk))

        # configure the graph
        configured = stuett.core.configuration(x, config)

        x_configured = configured.compute()

        assert x_configured == 14
    except Exception as e:
        print(e)
        pass
