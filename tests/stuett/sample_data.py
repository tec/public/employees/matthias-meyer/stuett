"""MIT License

Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich), Matthias Meyer


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

import datetime as dt

"""
Some example data which can be used by tests
"""

channels = ["EHE", "EHN", "EHZ"]
stations = ["MH36", "MH44", "MH48", "MH52", "MH54"]

start_time = dt.datetime(2017, 7, 14, 7, 7, 0, tzinfo=dt.timezone.utc)
end_time = dt.datetime(2017, 7, 14, 7, 7, 10, tzinfo=dt.timezone.utc)

offset = dt.timedelta(days=1)
config = {
    "channel": channels[0],
    "station": stations[0],
    "start_time": start_time,
    "end_time": end_time,
}

# TODO: use @pytest.mark.parametrize
