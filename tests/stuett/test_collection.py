"""MIT License

Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich), Matthias Meyer


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

import stuett
from pathlib import Path
import pandas as pd

test_data_dir = Path(__file__).absolute().parent.joinpath("..", "data")
stuett.global_config.set_setting("user_dir", test_data_dir.joinpath("user_dir/"))


def test_collector():
    filename = Path(test_data_dir).joinpath(
        "timeseries", "MH30_temperature_rock_2017.csv"
    )

    node = stuett.data.CsvSource(filename)
    minmax_rate2 = stuett.data.MinMaxDownsampling(rate=2, dim="time")

    print("creating delayed node")
    x = node(delayed=True)

    print("downsampled delayed node")
    downsampled = minmax_rate2(x, delayed=True)

    print("DataCollector node")
    data_paths = [x, downsampled]
    granularities = [stuett.to_timedelta(180, "s"), stuett.to_timedelta(2, "d")]
    collector_node = stuett.data.DataCollector(data_paths, granularities)

    print("Instatiating DataCollector node")
    request = {"start_time": "2017-08-01", "end_time": "2017-08-02"}
    path = collector_node(request=request)
    # print(type(path))

    print("Configuration node")

    import dask

    dsk, dsk_keys = dask.base._extract_graph_and_keys([path])
    print(dict(dsk))

    path = stuett.core.configuration(path, request)

    dsk, dsk_keys = dask.base._extract_graph_and_keys([path])
    print(dsk)

    print(type(path))
    print("executing delayed node")
    print(path.compute())

    # request = {'start_time':'2017-08-01 10:01:00', 'end_time':'2017-08-01 10:02:00'}
    # path = collector_node(request=request)
    # path = stuett.core.configuration(path,request)
    # print(path.compute())


test_collector()
