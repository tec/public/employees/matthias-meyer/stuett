"""MIT License

Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich), Matthias Meyer


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

import stuett
import datetime as dt
import xarray as xr
import numpy as np
import pandas as pd

from tests.stuett.sample_data import *

import pytest


def test_minmax():
    minmax_default = stuett.data.MinMaxDownsampling(dim="time")
    minmax_rate2 = stuett.data.MinMaxDownsampling(rate=2, dim="time")

    np.random.seed(123)
    da = xr.DataArray(
        np.random.rand(9, 2),
        [
            ("time", pd.date_range("2000-01-01", periods=9)),
            ("channel", ["EHE", "EHZ"]),
        ],
    )

    x = minmax_rate2(da)

    assert x.shape == (4, 2)
    assert x.mean() == 0.46291252327532006


@pytest.mark.slow
def test_minmax_seismic():

    seismic_source = stuett.data.SeismicSource(use_arclink=True)
    minmax_rate2 = stuett.data.MinMaxDownsampling(2, dim="time")

    x = seismic_source(config)
    x = minmax_rate2(x)

    x.compute()
    # TODO: proper test


def test_spectrogram():
    spectrogram = stuett.data.Spectrogram(nfft=2, stride=1, dim="time")

    np.random.seed(123)
    da = xr.DataArray(
        np.random.rand(9, 3),
        [
            ("time", pd.date_range("2000-01-01", periods=9)),
            ("channel", ["EHE", "EHZ", "EHN"]),
        ],
        attrs={"sampling_rate": 0.000011574},
    )

    x = spectrogram(da)


def test_lttbdownsampling():
    import lttb

    periods = 14
    rate = 2
    channels = ["EHE", "EHZ"]
    # channels = ["EHE"]
    lttb_node = stuett.data.LTTBDownsampling(rate=rate, dim="time")

    np.random.seed(123)
    da = xr.DataArray(
        np.arange(periods * len(channels)).reshape(periods, len(channels)),
        # np.random.rand(periods, len(channels)),
        [
            ("time", pd.date_range("2000-01-01", periods=periods)),
            ("channel", channels),
        ],
    )

    # generate comparison data
    n_out = da.sizes["time"] // rate
    values = np.zeros((len(da["time"]), len(da["channel"]) + 1))
    values[:, :-1] = da.values
    values[:, -1] = da["time"].values
    result_list = []
    for i, c in enumerate(da["channel"]):
        # select only one channel and one timestamp
        current = values[:, [-1, i]]
        current_lttb = lttb.downsample(current, n_out)
        result_list.append(current_lttb)

    x = lttb_node(da)

    # compare channel-wise if values are correct
    for i, c in enumerate(da["channel"]):
        continue
        # we can only compare values not timestamps
        print(x.values[:, i])
        print(result_list[i][:, 1])
        # TODO: currently we cannot compare to lttb implementatino
        #       due to different bucket generation
        # assert x.values[:,i] == result_list[i][:,1]

    # TODO: assert if timestamps are correct

    print(x)
