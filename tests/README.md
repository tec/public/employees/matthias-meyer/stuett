# stuett tests

Run tests with 

```
pytest tests/
pytest -s tests/                    # to see the prints
pytest -v -m "not slow" tests/      # to run without slow test, e.g. data retrieval test
```