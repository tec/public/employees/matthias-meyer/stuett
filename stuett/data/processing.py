"""MIT License

Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich), Matthias Meyer


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""


from ..global_config import get_setting
from ..core.graph import StuettNode

import dask
import numpy as np
import xarray as xr
import scipy.signal
import pandas as pd
import lttb
import warnings


class MinMaxDownsampling(StuettNode):
    def __init__(self, rate=1, dim=None):
        # dim is by default the last dimension
        # since we always choose two values (min and max) per bucket the
        # the internal downsampling rate must be of factor two larger than
        # the effective (and desired) downsampling rate
        self.rate = rate * 2
        self.dim = dim

    def forward(self, data=None, request=None):
        dim = self.dim
        if dim is None:
            dim = data.dims[-1]

        # rolling = data.rolling({dim:self.rate})
        # x_min = rolling.construct("buckets", stride=self.rate).min("time").rename({"buckets":"time"}).dropna('time')
        # x_max = rolling.construct("buckets", stride=self.rate).max("time").rename({"buckets":"time"}).dropna('time')

        rolling = data.rolling({dim: self.rate}, stride=self.rate)
        x_min = rolling.min().dropna(dim)
        x_max = rolling.max().dropna(dim)

        # TODO: better interleave instead of concat
        x_ds = xr.concat([x_min, x_max], dim)

        # TODO: try to avoid this by using interleaving
        x_ds = x_ds.sortby(dim)

        return x_ds


class LTTBDownsampling(StuettNode):
    def __init__(self, rate=1, dim=None):
        # Based on and many thanks to https://github.com/javiljoen/lttb.py
        self.rate = rate
        self.dim = dim

    def forward(self, data=None, request=None):
        dim = self.dim
        if dim is None:
            dim = data.dims[-1]
        """ The following tries to re-implement the LTTB algorithm for xarray
            There are several issues:
            1. We cannot use the LTTB package
               it expects the index and the data in one numpy array. Since our index is
               usually in a xarray coordinate, we would need to disassemble the xarray
               and then reassemble it. (As done in the current implementation).
               We would potentially loose some information from the original datastructure
               and we would need to assume certain datatypes for certain dimensions.
            2. For multi-dimensional arrays, we run into issues since since the dimension
               coordinate we want to reduce on applies to multiple axes and on each axis
               LTTB chooses a different pair of (index, value). Solution would be to choose
               the index for each window statically but choose the value depending on LTTB.
        """

        """ Note: The lttb implementation differs from our implementation
            due to different bucket generation. lttb uses numpy.array_split
            which subdivides into buckets of varying length ('almost' equal) and does not drop
            any values
            we use a rolling window which subdivides into equal lengths
            but drops values at the end

            form numpy docs for array_split:
            For an array of length l that should be split into n sections, it returns l % n sub-arrays of size l//n + 1 and the rest of size l//n.
        """

        # adapt downsampling rate
        n_out = data.sizes[dim] / self.rate
        # print('n_out',n_out)
        rate = np.ceil(data.sizes[dim] / (n_out - 2)).astype(np.int)
        # print(rate)
        # rate = data.sizes[dim] // (n_out-2)
        # rate = self.rate
        end = data.isel({dim: -1})
        data = data.isel({dim: slice(0, -1)})
        rolling = data.rolling(dim={dim: rate}, stride=rate)
        # print(rolling)
        # print(data.sizes['time']//self.rate)

        index_bins = (
            data[dim].rolling(dim={dim: rate}, stride=rate).construct("buckets")
        )
        # print('index',index_bins)

        value_bins = rolling.construct("buckets")
        # print('value',value_bins)

        # return
        # rolling_dim = utils.get_temp_dimname(self.obj.dims, "_rolling_dim")
        # windows = self.construct(rolling_dim, stride=self.stride)
        # result = windows.reduce(func, dim=rolling_dim, **kwargs)
        # # Find valid windows based on count.
        # counts = self._counts()
        # return result.where(counts >= self._min_periods)
        # value_mean = rolling.mean()

        value_mean = value_bins.mean("buckets")
        index_mean = index_bins.mean("buckets")
        # print(index_mean)
        out = []
        prev = None
        argmaxes = []
        for i in range(value_bins.sizes[dim]):
            current_value = value_bins.isel({dim: i})
            current_index = index_bins.isel({dim: i})
            # print(i,current_value.sizes['buckets'])
            # print('cv',current_value)
            if i == 0:
                # the first bucket consists of NaN and one entry
                # We choose the one entry and continue
                prev_value = current_value.isel(buckets=-1)
                prev_index = current_index.isel(buckets=-1)
                argmaxes.append(prev_value)
                continue

            # prev_data/time contains only one entry on `dim`-axis
            a = prev_value
            a_time = prev_index
            # current_data/time contains multiple entries on `dim`-axis
            bs = current_value
            bs_time = current_index
            if i < value_bins.sizes[dim] - 1:
                next_value = value_mean.isel({dim: i + 1})
                next_index = index_mean.isel({dim: i + 1})
            else:
                next_value = end
                next_index = end[dim]

            # calculate areas of triangle
            bs_minus_a = bs - a
            c_minus_p_value = current_value - prev_value
            c_minus_p_index = current_index - prev_index

            a_minus_bs = a - bs
            p_minus_c_value = prev_value - current_value
            p_minus_c_index = prev_index - current_index

            P_i, P_v = prev_index.astype(np.float), prev_value
            C_i, C_v = current_index.astype(np.float), current_value
            N_i, N_v = next_index.astype(np.float), next_value

            # print('P',P_i,P_v)
            # print('C',C_i,C_v)
            # print('N',N_i,N_v)
            # points P (prev), C (current), N (next)
            # Next is th emean
            # area = 0.5 * abs(P_i * (C_v - N_v) + C_i * (N_v - P_v) + N_i * (P_v - C_v))
            areas = 0.5 * abs(P_i * (C_v - N_v) + C_i * (N_v - P_v) + N_i * (P_v - C_v))

            # print(areas)
            # print(current_value)
            # print(current_value.shape)
            # print(areas.shape)
            # axis_num = current_value.get_axis_num('buckets')
            # arg = areas.argmax()
            # print('c',current_value)
            # print('oi',arg)
            try:
                arg = areas.argmax("buckets", skipna=False)
            except ValueError as e:
                warnings.warn(e)
                c = 0  # TODO: find a better solution to the ValueError("All-NaN slice encountered")

            c = current_value.isel({"buckets": arg})
            # print(c)
            argmaxes.append(c)

        argmaxes.append(end)
        array = xr.concat(argmaxes, "time")
        # print(array)

        # TODO: integrate nicely
        use_pkg = False
        if use_pkg:
            # This is quite hacky and works only if the the selected dimension is a datetime
            if len(data.squeeze().dims) > 1:
                raise RuntimeError("Can only work with arrays of one dimension")

            d = np.array(
                [data["time"].values.astype(np.int64), data.squeeze().values]
            ).T
            small_data = lttb.downsample(d, n_out=n_out)

            array = xr.DataArray(
                small_data[:, 1],
                dims=[dim],
                coords={dim: (dim, pd.to_datetime(small_data[:, 0]))},
            )

            array.attrs = data.attrs
        return array


class Downsampling(StuettNode):
    def __init__(self):
        raise NotImplementedError()
        # TODO: high level downsampling node which uses one of the other downsampling
        #       classes depending on the user request
        pass


class Spectrogram(StuettNode):
    def __init__(
        self,
        nfft=2048,
        stride=1024,
        dim=None,
        sampling_rate=None,
        window=("tukey", 0.25),
        detrend="constant",
        return_onesided=True,
        scaling="density",
        mode="psd",
    ):
        super().__init__(
            nfft=nfft,
            stride=stride,
            dim=dim,
            sampling_rate=sampling_rate,
            window=window,
            detrend=detrend,
            return_onesided=return_onesided,
            scaling=scaling,
            mode=mode,
        )

    def forward(self, data=None, request=None):
        config = self.config.copy()  # TODO: do we need a deep copy?
        if request is not None:
            config.update(request)

        if config["dim"] is None:
            config["dim"] = data.dims[-1]

        axis = data.get_axis_num(config["dim"])

        if config["sampling_rate"] is None:
            if "sampling_rate" not in data.attrs:
                raise RuntimeError(
                    "Please provide a sampling_rate attribute "
                    "to your config or your input data"
                )
            else:
                config["sampling_rate"] = data.attrs["sampling_rate"]

        samples = data.values
        noverlap = config["nfft"] - config["stride"]

        freqs, spectrum_time, spectrum = scipy.signal.spectrogram(
            samples,
            nfft=config["nfft"],
            nperseg=config["nfft"],
            noverlap=noverlap,
            fs=config["sampling_rate"],
            axis=axis,
            detrend=config["detrend"],
            scaling=config["scaling"],
            return_onesided=config["return_onesided"],
            mode=config["mode"],
            window=config["window"],
        )

        # TODO: check if this is what we want. it's: the earliest timestamp of input + the delta computed by scipy
        ds_coords = pd.to_datetime(
            pd.to_datetime(data[config["dim"]][0].values)
            + pd.to_timedelta(spectrum_time, "s")
        ).tz_localize(
            None
        )  # TODO: change when xarray #3291 is fixed

        # Create a new DataArray for the spectogram

        dims = (
            data.dims[:axis] + ("frequency",) + data.dims[axis + 1 :] + (config["dim"],)
        )
        coords = dict(data.coords)
        coords.update({"frequency": ("frequency", freqs), config["dim"]: ds_coords})
        dataarray = xr.DataArray(spectrum, dims=dims, coords=coords)

        return dataarray
