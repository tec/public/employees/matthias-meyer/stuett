"""MIT License

Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich), Matthias Meyer


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

from pandas import to_datetime, to_timedelta
from zarr import DirectoryStore, ABSStore

import dask
import pandas as pd
import io, codecs

# TODO: make it a proper decorator with arguments etc
def dat(x):
    """ Helper function to tranform input callable into 
        dask.delayed object

        From low german 'stütt dat!' which means "support it!"
    
    Arguments:
        x {callable} -- Any input callable which is supported
                        by dask.delayed
    
    Returns:
        dask.delayed object -- 
    """

    return dask.delayed(x)


def to_csv_with_store(store, filename, dataframe, pandas_kwargs=None):
    if pandas_kwargs is None:
        pandas_kwargs = dict()

    StreamWriter = codecs.getwriter("utf-8")
    bytes_buffer = io.BytesIO()
    string_buffer = StreamWriter(bytes_buffer)
    dataframe.to_csv(string_buffer, **pandas_kwargs)
    store[filename] = bytes_buffer.getvalue()


def read_csv_with_store(store, filename, pandas_kwargs=None):
    if pandas_kwargs is None:
        pandas_kwargs = dict()

    bytes_buffer = io.BytesIO(store[str(filename)])
    StreamReader = codecs.getreader("utf-8")
    string_buffer = StreamReader(bytes_buffer)
    return pd.read_csv(string_buffer)


def indexers_to_request(indexers):
    request = {"start_" + k: v.start for k, v in indexers.items()}
    request.update({"end_" + k: v.stop for k, v in indexers.items()})
    return request
