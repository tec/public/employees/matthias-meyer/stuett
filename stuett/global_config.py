"""MIT License

Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich), Matthias Meyer


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

import yaml
from os.path import dirname, abspath, join
import os
import appdirs
import warnings
import datetime as dt

# initialize global settings with certain default values
_GLOBAL_CONFIG_DICT = {
    "permasense_server": "http://data.permasense.ch/",
    "metadata_directory": join(dirname(abspath(__file__)), "metadata"),
    "reference_time": dt.datetime(2000, 1, 1, 0, 0, 0),
}


def get_global_config():
    return _GLOBAL_CONFIG_DICT


def setting_exists(key):
    return key in _GLOBAL_CONFIG_DICT


def get_setting(key):
    """Returns the global setting for the given key
    
    Arguments:
        key {sting} -- setting name
    
    Returns:
        [type] -- setting value
    """

    try:
        return _GLOBAL_CONFIG_DICT[key]
    except KeyError:
        print(
            f"""The stuett setting {key} was not found and is required by a function call.
        Set it before your first call to the stuett package.
        This can be done by either providing a settings file via stuett.load_config(),
        updating your user config file or updating the settings directly with
               stuett.set_setting('{key}',value)"""
        )


def get_setting_path(key):
    """Get global setting and make sure it is a valid path
    
    Arguments:
        key {string} -- setting name 

    Returns:
        [type] -- setting value
    """
    path = join(get_setting(key), "")
    if not os.path.isdir(path):
        warnings.warn("stuett requested a path which is invalid: {}".format(path))
    return path


def set_setting(key, value):
    _GLOBAL_CONFIG_DICT[key] = value


def load_config(filename):
    """Load settings from a yaml file.

    This is used to setup the workspace and make the module
    be able to find the necessary permasense directories

    The settings file can for example contain the following items:
    
    ```python
        permasense_vault_dir: '/path/to/permasense_vault/'
        user_dir: '/path/to/user_dir/'
    ```

    """

    if os.path.isfile(filename):
        with open(filename, "r") as f:
            settings = yaml.safe_load(f)
    else:
        raise IOError("Parameter file not found [%s]" % filename)

    _GLOBAL_CONFIG_DICT.update(settings)


def get_user_config_file():
    return join(appdirs.AppDirs("stuett", "stuett").user_config_dir, "config.yml")


def load_user_config():
    """Check if a user specific config exists and load it
    """

    user_config = get_user_config_file()

    if os.path.isfile(user_config):
        load_config(user_config)


def get_global_time():
    return dt.datetime(2000, 1, 1, 0, 0, 0)


load_user_config()  # intially load the user settings
